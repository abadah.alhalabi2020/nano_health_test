// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:nano_health_test/core/constants/enums.dart';
import 'package:nano_health_test/core/dependency_injection/injection_container.dart';
import 'package:nano_health_test/core/errors/api_exceptions.dart';
import 'package:nano_health_test/core/network_info/network_info.dart';
import 'package:nano_health_test/core/session_management/session.dart';

import 'domain_url.dart';

class HttpProvider {
  final http.Client client;
  final NetworkInfoImpl networkInfo;
  HttpProvider(
    this.client,
    this.networkInfo,
  );
  //
  static const int TIME_OUT_DURATION = 20;
  //
  Future<dynamic> goApi({
    required String url,
    required ApiMethod method,
    dynamic body,
    Map<String, String>? additionalHeader,
  }) async {
    //
    //
    final baseHeader = {
      'Accept': 'application/json',
      'lang': 'ar',
      'Authorization': 'Bearer ${getIt<Session>().getToken()}',
      'Content-Type': 'application/json',
    };
    //
    http.Response? response;
    //
    baseHeader.addAll(additionalHeader ?? {});
    //
    try {
      if (!(await networkInfo.isConnected)) {
        throw ApiNotRespondingException('error_not_responsed');
      }
      switch (method) {
        //
        case ApiMethod.get:
          response = await client
              .get(
                Uri.parse(
                  BaseUrl + url,
                ),
                headers: baseHeader,
              )
              .timeout(const Duration(seconds: TIME_OUT_DURATION));

          break;
        //
        case ApiMethod.post:
          response = await client
              .post(
                Uri.parse(
                  BaseUrl + url,
                ),
                headers: baseHeader,
                body: body,
              )
              .timeout(const Duration(seconds: TIME_OUT_DURATION));

          break;

        case ApiMethod.put:
          response = await client
              .put(
                Uri.parse(
                  BaseUrl + url,
                ),
                headers: baseHeader,
                body: body,
              )
              .timeout(const Duration(seconds: TIME_OUT_DURATION));

          break;

        case ApiMethod.delete:
          response = await client
              .delete(
                Uri.parse(
                  BaseUrl + url,
                ),
                headers: baseHeader,
              )
              .timeout(const Duration(seconds: TIME_OUT_DURATION));
          break;

        default:
      }
    } on SocketException {
      throw ApiNotRespondingException('error_not_responsed');
    } on TimeoutException {
      throw ApiNotRespondingException('error_not_responsed');
    } catch (e) {
      log(e.toString());
      throw FetchDataException('error_server');
    }
    // inspect(jsonDecode(response!.body));
    // final res = (jsonDecode(response.body));

    switch (response!.statusCode) {
      case 200:
      case 201:
      case 202:
      case 203:
        return (jsonDecode(response.body));
      case 400:
      case 422:
      case 401:
        throw InvalidInputException('error_input');
      case 403:
        throw UnauthorisedException('error_unauth');
      case 500:
        throw FetchDataException('error_server');
      default:
        throw FetchDataException('error_server');
    }
  }
}
