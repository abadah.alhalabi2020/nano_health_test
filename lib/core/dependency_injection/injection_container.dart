import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:nano_health_test/features/products/domain/usecases/get_products_usecase.dart';
import 'package:nano_health_test/features/products/domain/usecases/get_products_usecase_details.dart';
import 'package:nano_health_test/features/products/presentation/bloc/products_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../features/auth/data/datasources/auth_datasource.dart';
import '../../features/auth/data/repositories/auth_repositories_impl.dart';
import '../../features/auth/domain/repositories/auth_repositories.dart';
import '../../features/auth/domain/usecases/login_usecase.dart';
import '../../features/auth/presentaion/cubits/auth_cubit.dart';
import '../../features/products/data/datasources/Products_datasource.dart';
import '../../features/products/data/repositories/products_repositories_impl.dart';
import '../../features/products/domain/repositories/Products_repositories.dart';
import '../apis/http_provider.dart';
import '../network_info/network_info.dart';
import '../session_management/session.dart';
import '../session_management/session_impl.dart';

final getIt = GetIt.instance;

Future<void> init() async {
  getIt.registerFactory(
    () => AuthCubit(
      loginUsecase: getIt(),
    ),
  );
  getIt.registerLazySingleton(() => LoginUsecase(getIt()));
  getIt.registerLazySingleton<AuthRepositories>(
      () => AuthRepositoriesImpl(authDataResource: getIt()));

// Datasources
  getIt.registerLazySingleton<AuthDataResource>(
      () => AuthDataResourceImpl(httpProvider: getIt()));
  getIt.registerFactory(
    () => ProductsBloc(
        getProductsDetailsUsecase: getIt(), getProductsUsecase: getIt()),
  );
  getIt.registerLazySingleton(() => GetProductsUsecase(getIt()));
  getIt.registerLazySingleton(() => GetProductsDetailsUsecase(getIt()));
  getIt.registerLazySingleton<ProductsRepositories>(
      () => ProductsRepositoriesImpl(productsDataResource: getIt()));

// Datasources
  getIt.registerLazySingleton<ProductsDataResource>(
      () => ProductsDataResourceImpl(httpProvider: getIt()));
  //
  getIt.registerLazySingleton(() => NetworkInfoImpl(getIt()));
  getIt.registerLazySingleton(() => HttpProvider(getIt(), getIt()));
  getIt.registerLazySingleton(http.Client.new);
//! External
  final sharedPreferencesInstance = await SharedPreferences.getInstance();
  getIt.registerLazySingleton(() => sharedPreferencesInstance);
  getIt.registerLazySingleton<Session>(() => SessionImpl(getIt()));
  getIt.registerLazySingleton(InternetConnectionChecker.new);
  final navigatorKey = GlobalKey<NavigatorState>();

  getIt.registerLazySingleton<GlobalKey<NavigatorState>>(() => navigatorKey);
}
