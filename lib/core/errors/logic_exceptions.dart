import 'package:nano_health_test/core/errors/api_exceptions.dart';

class LogicException extends AppException {
  LogicException([String? message])
      : super(message, "Error executing a function");
}
