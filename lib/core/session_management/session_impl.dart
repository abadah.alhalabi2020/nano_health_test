import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'session.dart';

class SessionImpl implements Session {
  final SharedPreferences sharedPref;

  SessionImpl(this.sharedPref);

  @override
  String getToken() {
    return sharedPref.getString("token") ?? '';
  }

  @override
  void setToken(String token) {
    sharedPref.setString("token", token);
  }
  //
}
