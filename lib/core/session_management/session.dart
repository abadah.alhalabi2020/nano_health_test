
abstract class Session {
  String getToken();
  void setToken(String token);  
}
