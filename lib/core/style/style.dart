import 'package:flutter/material.dart';

Color primaryColor = const Color(0xFF2AB3C6);
//
Color secondryColor = const Color(0xFF188095);
//
Color thirdColor = const Color(0xFF08293B);
//
Color secondTextColor = const Color(0xFFEDEDED);
//
Color txtColor = Colors.white;
//
Color subTitleColor = const Color(0xFF4D4D4D);
//
Color colorBackground = const Color(0x00fafafa);
//
TextStyle textstyle_22 = TextStyle(
  color: txtColor,
  fontSize: 22,
);

TextStyle textstyle_18 = const TextStyle(
  fontSize: 18,
);
TextStyle textstyle_17 = TextStyle(
  color: txtColor,
  fontSize: 17,
);
TextStyle textstyle_20 = TextStyle(
  color: txtColor,
  fontSize: 20,
);

TextStyle textstyle_16 = TextStyle(
  color: txtColor,
  fontSize: 16,
);
TextStyle textstyle_15 = TextStyle(
  color: txtColor,
  fontSize: 15,
);

TextStyle textstyle_12 = TextStyle(
  color: txtColor,
  fontSize: 12,
);
TextStyle textstyle_10 = TextStyle(
  color: txtColor,
  fontSize: 10,
);

TextStyle textstyle_14 = TextStyle(
  color: txtColor,
  fontSize: 14,
);

TextStyle textstylewhite_16 = const TextStyle(
  color: Colors.white,
  fontSize: 16,
);
TextStyle textstylewhite_15 = const TextStyle(
  color: Colors.white,
  fontSize: 15,
);

TextStyle textstylewhite_12 = const TextStyle(
  color: Colors.white,
  fontSize: 12,
);
TextStyle textstylewhite_14 = const TextStyle(
  color: Colors.white,
  fontSize: 14,
);

TextStyle textstylewhite_10 = const TextStyle(
  color: Colors.white,
  fontSize: 10,
);

TextStyle textstylewhite_18 = const TextStyle(
  color: Colors.white,
  fontSize: 18,
);

// add shadow style to container
BoxDecoration boxdecoration = BoxDecoration(
  color: Colors.white,
  borderRadius: const BorderRadius.only(
    bottomLeft: Radius.circular(18),
    bottomRight: Radius.circular(18),
  ),
  boxShadow: [
    BoxShadow(
      color: Colors.black.withOpacity(0.1),
      blurRadius: 6,
      offset: const Offset(0, 3), // changes position of shadow
    ),
  ],
);

ButtonStyle btnStyle = ButtonStyle(
  // backgroundColor: MaterialStateProperty.all(value: primaryColor),
  backgroundColor: MaterialStateProperty.all(Colors.transparent),
  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
    RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(10),
    ),
  ),
  shadowColor: MaterialStateProperty.all(Colors.transparent),
);
