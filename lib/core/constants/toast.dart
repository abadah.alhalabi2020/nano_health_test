import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

myToast(m) {
  BotToast.showText(
    text: '$m',
    align: Alignment.center,
  );
}
