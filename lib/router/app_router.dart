import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:nano_health_test/core/dependency_injection/injection_container.dart';
import 'package:nano_health_test/core/session_management/session.dart';
import 'package:nano_health_test/features/auth/presentaion/pages/signin.dart';
import 'package:nano_health_test/features/products/presentation/bloc/products_bloc.dart';

import '../features/auth/presentaion/cubits/auth_cubit.dart';
import '../features/products/presentation/pages/all_products.dart';
import '../features/products/presentation/pages/product_details.dart';

class AppRouter {
  static final GoRouter goRoute = GoRouter(
    navigatorKey: getIt<GlobalKey<NavigatorState>>(),
    debugLogDiagnostics: true,
    initialLocation: getIt<Session>().getToken().isEmpty ? '/signin' : '/',
    routes: [
      GoRoute(
        path: "/",
        name: "root",
        builder: (context, state) => BlocProvider(
          create: (context) => getIt<ProductsBloc>()..add(GetProductsEvent()),
          child: Builder(builder: (context) {
            return const ProductsView();
          }),
        ),
        routes: [
          GoRoute(
            path: "product_details",
            name: "product_details",
            builder: (context, state) => BlocProvider<ProductsBloc>.value(
              value: state.extra as ProductsBloc
                ..add(
                  GetProductsDetailEvent(
                    id: int.tryParse(state.queryParameters['id']!),
                  ),
                ),
              child: Builder(builder: (context) {
                return const ProductDetailsView();
              }),
            ),
          ),
        ],
      ),
      GoRoute(
        path: "/signin",
        name: "signin",
        builder: (context, state) => BlocProvider(
          create: (context) => getIt<AuthCubit>(),
          child: Builder(builder: (context) {
            return const SignInWidget();
          }),
        ),
      ),
    ],
  );
}
