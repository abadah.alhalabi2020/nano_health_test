import 'dart:io';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nano_health_test/router/app_router.dart';
import 'core/dependency_injection/injection_container.dart' as di;

import 'core/style/style.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await EasyLocalization.ensureInitialized();
  await Future.wait(
    [
      di.init(),
      SystemChrome.setPreferredOrientations(
        [
          DeviceOrientation.portraitUp,
          DeviceOrientation.portraitDown,
        ],
      ),
    ],
  );
  //
  HttpOverrides.global = MyHttpOverrides();
  final botToastBuilder = BotToastInit();
  runApp(MyApp(botToastBuilder));
}

class MyApp extends StatelessWidget {
  const MyApp(this.botToastBuilder, {super.key});
  final botToastBuilder;
  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routerConfig: AppRouter.goRoute,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: primaryColor,
        textTheme: const TextTheme(
          bodySmall: TextStyle(
            color: Colors.white,
            fontSize: 14,
            fontWeight: FontWeight.w400,
          ),
        ),
        colorScheme: ColorScheme.fromSwatch()
            .copyWith(secondary: Colors.white, primary: primaryColor),
      ),
      builder: (context, child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          child: botToastBuilder!(
            context,
            child!,
          ),
        );
      },
    );
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
