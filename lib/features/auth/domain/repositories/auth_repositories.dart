import 'package:dartz/dartz.dart';

abstract class AuthRepositories {
  Future<Unit> login(String phone, String otp);
}
