
import '../repositories/auth_repositories.dart';

class LoginUsecase {
  final AuthRepositories authRepositories;
  LoginUsecase(
    this.authRepositories,
  );
  Future<void> call(String phone, String password) async {
    try {
      await authRepositories.login(phone, password);
    } catch (e) {
      throw Exception(e);
    }
  }
}