import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';

import '../../../../core/style/style.dart';
import '../cubits/auth_cubit.dart';

class SignInWidget extends StatefulWidget {
  const SignInWidget({
    super.key,
  });

  @override
  State<SignInWidget> createState() => _SignInWidgetState();
}

class _SignInWidgetState extends State<SignInWidget> {
  TextEditingController? emailController;
  TextEditingController? passwordController;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  @override
  void initState() {
    emailController = TextEditingController();
    passwordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    emailController!.dispose();
    passwordController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollView(
      controller: ScrollController(keepScrollOffset: false),
      physics: const BouncingScrollPhysics(),
      headerSliverBuilder: (context, innerBoxIsScrolled) {
        return [
          SliverAppBar(
            backgroundColor: primaryColor,
            pinned: true,
            centerTitle: true,
            expandedHeight: 300,
            floating: true,
            stretch: true,
            flexibleSpace: FlexibleSpaceBar(
              collapseMode: CollapseMode.parallax,
              centerTitle: false,
              title: Align(
                alignment: Alignment.bottomLeft,
                child: Text(
                  "Log In",
                  style: textstyle_18.copyWith(color: Colors.white),
                ),
              ),
              expandedTitleScale: 1.5,
              titlePadding:
                  const EdgeInsets.only(bottom: 15, top: 10, left: 15),
              stretchModes: const [
                StretchMode.fadeTitle,
                StretchMode.zoomBackground,
              ],
              background: Container(
                padding: const EdgeInsets.symmetric(
                    // horizontal: 50,
                    // vertical: 80,
                    ),
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Color(
                          0xFF188095), // Replace with your desired color codes
                      Color(0xFF2AB3C6),
                    ],
                    begin: Alignment.bottomLeft,
                    end: Alignment.bottomRight,
                  ),
                ),
                // height: 150,
                // width: double.infinity,
                child: SvgPicture.asset(
                  'assets/logo.svg',
                  fit: BoxFit.none,
                  height: 50,
                  width: 50,
                ),
              ),
            ),
          ),
        ];
      },
      body: Material(
        child: BlocListener<AuthCubit, AuthState>(
          listener: (context, state) {
            if (state.hasError) {
              showDialog(
                context: context,
                builder: (context) => AlertDialog(
                  title: const Text("Error"),
                  content: Text(state.error ?? "Something went wrong"),
                  actions: [
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text("OK"),
                    ),
                  ],
                ),
              );
            } else if (state.success) {
              context.go('/');
            }
          },
          listenWhen: (previous, current) {
            return previous.isLoading != current.isLoading;
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 35),
            child: Form(
              key: formKey,
              child: Column(
                children: [
                  TextFormField(
                    controller: emailController,
                    decoration: const InputDecoration(
                      labelText: "Email",
                      hintText: "Enter your email",
                    ),
                    style: textstyle_18.copyWith(color: Colors.black),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return "Please enter your email";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    controller: passwordController,
                    decoration: const InputDecoration(
                      labelText: "Password",
                      hintText: "Enter your password",
                    ),
                    style: textstyle_18.copyWith(color: Colors.black),
                    validator: (value) {
                      if (value?.isEmpty ?? true) {
                        return "Please enter your password";
                      }
                      return null;
                    },
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      FocusScope.of(context).unfocus();
                      if (formKey.currentState?.validate() ?? false) {
                        context.read<AuthCubit>().login(
                              email: emailController!.text,
                              password: passwordController!.text,
                            );
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: primaryColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(62),
                      ),
                      padding: const EdgeInsets.symmetric(
                        horizontal: 50,
                        vertical: 20,
                      ),
                    ),
                    child: Center(
                        child: BlocSelector<AuthCubit, AuthState, bool>(
                      selector: (state) {
                        return state.isLoading;
                      },
                      builder: (context, state) {
                        return state
                            ? const CupertinoActivityIndicator(
                                color: Colors.white,
                              )
                            : Text(
                                "Continue",
                                style: textstyle_16,
                              );
                      },
                    )),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextButton(
                      onPressed: () {},
                      child: Text(
                        "NEED HELP ?",
                        style: textstyle_15.copyWith(color: Colors.black),
                      )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
