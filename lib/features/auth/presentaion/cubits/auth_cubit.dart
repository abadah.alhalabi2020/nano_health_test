import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:nano_health_test/features/auth/domain/usecases/login_usecase.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  LoginUsecase loginUsecase;
  AuthCubit({
    required this.loginUsecase,
  }) : super(AuthState.initState());
  //
  Future login({required String email, required String password}) async {
    emit(state.copyWith(isLoading: true, error: "", hasError: false));
    try {
      await loginUsecase(email, password).then(
          (value) => emit(state.copyWith(isLoading: false, success: true)));
    } catch (e) {
      emit(state.copyWith(
          error: "Something went wrong", isLoading: false, hasError: true));
    } finally {
      emit(state.copyWith(isLoading: false));
    }
  }
}
