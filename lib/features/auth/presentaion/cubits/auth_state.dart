// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'auth_cubit.dart';

class AuthState extends Equatable {
  //
  final bool isLoading;
  final bool hasError;
  final String error;
  final bool success;
  //
  const AuthState({
    required this.isLoading,
    required this.hasError,
    required this.error,
    this.success = false,
  });

  @override
  List<Object?> get props => [
        isLoading,
        hasError,
        error,
        success,
      ];
  factory AuthState.initState() {
    return const AuthState(
      isLoading: false,
      hasError: false,
      error: '',
      success: false,
    );
  }

  AuthState copyWith({
    bool? isLoading,
    bool? hasError,
    String? error,
    bool? success,
  }) {
    return AuthState(
      isLoading: isLoading ?? this.isLoading,
      hasError: hasError ?? this.hasError,
      error: error ?? this.error,
      success: success ?? this.success,
    );
  }
}
