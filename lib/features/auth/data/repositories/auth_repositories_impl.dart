
import 'package:dartz/dartz.dart';
import 'package:nano_health_test/features/auth/data/datasources/auth_datasource.dart';

import '../../domain/repositories/auth_repositories.dart';

class AuthRepositoriesImpl implements AuthRepositories {
  AuthDataResource authDataResource;

  AuthRepositoriesImpl({
    required this.authDataResource,
  });
  

  @override
  Future<Unit> login(String phone, String password) async {
    final response = await authDataResource.login(phone, password);
    return response;
  }
}
