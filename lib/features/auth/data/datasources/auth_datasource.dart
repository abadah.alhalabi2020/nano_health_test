import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:nano_health_test/core/constants/enums.dart';

import '../../../../core/apis/http_provider.dart';
import '../../../../core/dependency_injection/injection_container.dart';
import '../../../../core/session_management/session.dart';

abstract class AuthDataResource {
  Future<Unit> login(String phone, String password);
}

class AuthDataResourceImpl implements AuthDataResource {
  AuthDataResourceImpl({required this.httpProvider});
  final HttpProvider httpProvider;

  @override
  Future<Unit> login(String email, String password) async {
    try {
      var response = await httpProvider.goApi(
        url: 'auth/login',
        method: ApiMethod.post,
        body: jsonEncode({'username': email, 'password': password}),
      );
      getIt<Session>().setToken(response!['token']);
    } catch (e) {
      throw Exception(e);
    }

    return unit;
  }
}
