import 'package:flutter/material.dart';
import 'package:nano_health_test/features/products/domain/entities/product_entity.dart';

import '../../../../core/style/style.dart';

class ProductCard extends StatelessWidget {
  ProductCard({super.key, required this.productEntity});
  ProductEntity productEntity;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 223,
          child: Stack(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Image.network(
                  productEntity.image!,
                  fit: BoxFit.cover,
                  width: double.infinity,
                  loadingBuilder: (BuildContext context, Widget child,
                      ImageChunkEvent? loadingProgress) {
                    if (loadingProgress == null) return child;
                    return const Center(
                      child: CircularProgressIndicator(
                        color: Colors.black,
                      ),
                    );
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 140),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: Transform.translate(
                    offset: const Offset(0, 40),
                    child: const VtGradient(
                      [
                        Colors.transparent,
                        Colors.black,
                      ],
                      [0, 1],
                      // height: 78,
                      // width: 363,
                      blendMode: BlendMode.srcOver,
                    ),
                  ),
                ),
              ),
              Positioned(
                bottom: 0,
                width: 375,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        productEntity.price.toString(),
                        style: textstyle_22.copyWith(color: Colors.white),
                      ),
                      buildRatingStars(productEntity.rating!.rate!),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          productEntity.title!,
          style: textstyle_18.copyWith(color: Colors.black),
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          productEntity.description!,
          style: textstyle_14.copyWith(color: Colors.black),
        ),
      ],
    );
  }
}

class VtGradient extends GradientContainer {
  const VtGradient(List<Color> colors, List<double> stops,
      {Key? key,
      Widget? child,
      double? width,
      double? height,
      Alignment? alignment,
      BlendMode? blendMode,
      BorderRadius? borderRadius})
      : super(colors, stops,
            key: key,
            child: child,
            width: width,
            height: height,
            alignment: alignment,
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            blendMode: blendMode,
            borderRadius: borderRadius);
}

class GradientContainer extends StatelessWidget {
  const GradientContainer(this.colors, this.stops,
      {Key? key,
      this.child,
      this.width,
      this.height,
      this.alignment,
      this.begin,
      this.end,
      this.blendMode,
      this.borderRadius})
      : super(key: key);
  final List<Color> colors;
  final List<double> stops;
  final double? width;
  final double? height;
  final Widget? child;
  final Alignment? begin;
  final Alignment? end;
  final Alignment? alignment;
  final BlendMode? blendMode;
  final BorderRadius? borderRadius;

  @override
  Widget build(BuildContext context) => IgnorePointer(
        child: Container(
          width: width,
          height: height,
          alignment: alignment,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: begin ?? Alignment.centerLeft,
              end: end ?? Alignment.centerRight,
              colors: colors,
              stops: stops,
            ),
            backgroundBlendMode: blendMode,
            borderRadius: borderRadius,
          ),
          child: child,
        ),
      );
}

Row buildRatingStars(num rating) {
  List<Widget> stars = [];

  for (int i = 0; i < rating.floor(); i++) {
    stars.add(const Icon(Icons.star, color: Colors.yellow));
  }

  if (rating % 1 != 0) {
    stars.add(const Icon(Icons.star_half, color: Colors.yellow));
  }

  for (int i = stars.length; i < 5; i++) {
    stars.add(const Icon(Icons.star_border, color: Colors.yellow));
  }

  return Row(children: stars);
}
