import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:nano_health_test/features/products/domain/usecases/get_products_usecase.dart';
import 'package:nano_health_test/features/products/domain/usecases/get_products_usecase_details.dart';

import '../../domain/entities/product_entity.dart';

part 'products_event.dart';
part 'products_state.dart';

class ProductsBloc extends Bloc<ProductsEvent, ProductsBlocState> {
  GetProductsDetailsUsecase getProductsDetailsUsecase;
  GetProductsUsecase getProductsUsecase;

  ProductsBloc({
    required this.getProductsDetailsUsecase,
    required this.getProductsUsecase,
  }) : super(ProductsBlocState.initProductsBlocState()) {
    on<ProductsEvent>(
      (event, emit) async {
        if (event is GetProductsEvent) {
          emit(state.copyWith(isLoading: true));
          try {
            final response = await getProductsUsecase();
            emit(state.copyWith(isLoading: false, products: response));
          } catch (e) {
            emit(state.copyWith(isLoading: false, error: e.toString()));
          }
        } else if (event is GetProductsDetailEvent) {
          emit(state.copyWith(isLoading: true));
          try {
            final response = await getProductsDetailsUsecase(id: event.id!);
            emit(state.copyWith(isLoading: false, productDetails: response));
          } catch (e) {
            emit(state.copyWith(isLoading: false, error: e.toString()));
          }
        } else if (event is DeleteProductDetailEvent) {
          emit(state.copyWith(productDetails: const ProductEntity()));
        }
      },
    );
  }
}
