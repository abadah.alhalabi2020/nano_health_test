part of 'products_bloc.dart';

@immutable
abstract class ProductsEvent {}

class GetProductsEvent extends ProductsEvent {}

class GetProductsDetailEvent extends ProductsEvent {
  final int? id;

  GetProductsDetailEvent({
    this.id,
  });
}

class DeleteProductDetailEvent extends ProductsEvent {
  DeleteProductDetailEvent();
}
