part of 'products_bloc.dart';

class ProductsBlocState extends Equatable {
  final List<ProductEntity>? products;
  final bool? isLoading;
  final String? error;
  final bool? hasError;
  final ProductEntity? productDetails;

  const ProductsBlocState({
    this.products,
    this.isLoading,
    this.error,
    this.hasError,
    this.productDetails,
  });

  @override
  List<Object?> get props => [
        products,
        isLoading,
        error,
        hasError,
        productDetails,
      ];

  ProductsBlocState copyWith({
    List<ProductEntity>? products,
    bool? isLoading,
    String? error,
    bool? hasError,
    ProductEntity? productDetails,
  }) {
    return ProductsBlocState(
      products: products ?? this.products,
      isLoading: isLoading ?? this.isLoading,
      error: error ?? this.error,
      hasError: hasError ?? this.hasError,
      productDetails: productDetails ?? this.productDetails,
    );
  }

  factory ProductsBlocState.initProductsBlocState() {
    return const ProductsBlocState(
      products: [],
      isLoading: false,
      error: '',
      hasError: false,
      productDetails: null,
    );
  }
}
