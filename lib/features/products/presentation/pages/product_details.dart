import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bounce/flutter_bounce.dart';
import 'package:go_router/go_router.dart';
import 'package:nano_health_test/features/products/domain/entities/product_entity.dart';
import 'package:nano_health_test/features/products/presentation/bloc/products_bloc.dart';
import 'package:shimmer/shimmer.dart';

import '../../../../core/style/style.dart';
import '../widgets/product_card.dart';

class ProductDetailsView extends StatefulWidget {
  const ProductDetailsView({
    Key? key,
  }) : super(key: key);

  @override
  State<ProductDetailsView> createState() => _ProductDetailsViewState();
}

class _ProductDetailsViewState extends State<ProductDetailsView> {
  bool isExpanded = false;
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.read<ProductsBloc>().add(DeleteProductDetailEvent());
        return true;
      },
      child: Center(
        child: Scaffold(
          body: Stack(
            children: [
              BlocSelector<ProductsBloc, ProductsBlocState, ProductEntity?>(
                selector: (s) => s.productDetails,
                builder: (context, product) {
                  return product?.image?.isEmpty ?? true
                      ? const Center(child: CircularProgressIndicator())
                      : Image.network(product!.image!);
                },
              ),
              BlocListener<ProductsBloc, ProductsBlocState>(
                listener: (context, state) {
                  if (state.error?.isNotEmpty ?? false) {
                    context.pop();
                  }
                },
                listenWhen: (previous, current) =>
                    previous.isLoading != current.isLoading,
                child: BlocBuilder<ProductsBloc, ProductsBlocState>(
                  builder: (context, state) {
                    return AnimatedContainer(
                      duration: const Duration(milliseconds: 300),
                      margin: EdgeInsets.only(top: isExpanded ? 500 : 600),
                      padding: const EdgeInsets.all(20),
                      decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius:
                            BorderRadius.vertical(top: Radius.circular(35)),
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromRGBO(107, 127, 153, 0.25),
                            offset: Offset(0, 0),
                            blurRadius: 15,
                          ),
                        ],
                      ),
                      child: SingleChildScrollView(
                        clipBehavior: Clip.none,
                        physics: const NeverScrollableScrollPhysics(),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Bounce(
                              onPressed: () {
                                setState(() {
                                  isExpanded = !isExpanded;
                                });
                              },
                              duration: const Duration(milliseconds: 100),
                              child: Icon(
                                isExpanded
                                    ? Icons.keyboard_arrow_down_rounded
                                    : Icons.keyboard_arrow_up_rounded,
                                color: Colors.grey,
                                size: 30,
                              ),
                            ),
                            const SizedBox(height: 10),
                            Row(
                              // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  child: Container(
                                    padding: const EdgeInsets.all(13),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      boxShadow: const [
                                        BoxShadow(
                                          color: Colors.grey,
                                          blurRadius: 10,
                                          offset: Offset(0, 5),
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(20),
                                    ),
                                    child: Icon(
                                      Icons.ios_share_rounded,
                                      color: primaryColor,
                                      size: 30,
                                    ),
                                  ),
                                ),
                                const SizedBox(width: 20),
                                Expanded(
                                  flex: 4,
                                  child: ElevatedButton(
                                    onPressed: () {},
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: primaryColor,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(43),
                                      ),
                                      padding: const EdgeInsets.symmetric(
                                        horizontal: 50,
                                        vertical: 16,
                                      ),
                                    ),
                                    child: Center(
                                      child: Text(
                                        "Order Now",
                                        style: textstyle_16,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                            const SizedBox(height: 10),
                            SizedBox(
                              height: isExpanded ? 90 : 75,
                              child: state.isLoading ?? true
                                  ? Shimmer(
                                      gradient: const LinearGradient(
                                          colors: [
                                            Colors.white,
                                            Colors.grey,
                                          ],
                                          begin: Alignment.centerLeft,
                                          end: Alignment.centerRight),
                                      child: Text(
                                        'lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
                                        maxLines: isExpanded ? 10 : 3,
                                        style: textstyle_14.copyWith(
                                          color: Colors.grey,
                                          height: 1.5,
                                        ),
                                      ),
                                    )
                                  : Text(
                                      state.productDetails?.description ?? "",
                                      maxLines: isExpanded ? 10 : 3,
                                      style: textstyle_14.copyWith(
                                        color: Colors.grey,
                                        height: 1.5,
                                      ),
                                    ),
                            ),
                            isExpanded
                                ? const SizedBox(height: 20)
                                : const SizedBox.shrink(),
                            isExpanded
                                ? Container(
                                    padding: const EdgeInsets.all(13),
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      boxShadow: const [
                                        BoxShadow(
                                          color: Colors.grey,
                                          blurRadius: 10,
                                          offset: Offset(0, 5),
                                        ),
                                      ],
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    child: Center(
                                      child: Column(
                                        children: [
                                          Align(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              'Reviews (${state.productDetails?.rating?.count.toString() ?? " "})',
                                              style: textstyle_15.copyWith(
                                                  color: Colors.grey),
                                            ),
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(state.productDetails?.rating
                                                      ?.rate
                                                      .toString() ??
                                                  " "),
                                              buildRatingStars(state
                                                  .productDetails
                                                  ?.rating
                                                  ?.rate),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                            isExpanded
                                ? const SizedBox(height: 20)
                                : const SizedBox.shrink(),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
              Positioned(
                // width: MediaQuery.of(context).size.width,
                top: 80,
                left: 10,
                right: 10,
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Bounce(
                        onPressed: () {
                          context.pop();
                        },
                        duration: const Duration(milliseconds: 100),
                        child: Container(
                          padding: const EdgeInsets.all(13),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Icon(
                            Icons.arrow_back,
                            color: primaryColor,
                            size: 30,
                          ),
                        ),
                      ),
                      const SizedBox(width: 20),
                      Bounce(
                        onPressed: () {},
                        duration: const Duration(milliseconds: 100),
                        child: Container(
                          padding: const EdgeInsets.all(13),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Icon(
                            Icons.more_vert_rounded,
                            color: primaryColor,
                            size: 30,
                          ),
                        ),
                      ),
                    ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
