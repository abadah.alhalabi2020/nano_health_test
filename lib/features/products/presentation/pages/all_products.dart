import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bounce/flutter_bounce.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:go_router/go_router.dart';
import 'package:nano_health_test/core/style/style.dart';
import 'package:nano_health_test/features/products/presentation/bloc/products_bloc.dart';

import '../widgets/product_card.dart';

class ProductsView extends StatelessWidget {
  const ProductsView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(35),
          ),
        ),
        title: Text(
          "All Products",
          style: textstyle_20.copyWith(color: Colors.black),
        ),
        backgroundColor: Colors.white,
      ),
      body: BlocListener<ProductsBloc, ProductsBlocState>(
        listener: (context, state) {
          if (state.error?.isNotEmpty ?? false) {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text(state.error! ?? 'Something went wrong !')));
          }
        },
        listenWhen: (previous, current) =>
            previous.isLoading != current.isLoading,
        child: BlocBuilder<ProductsBloc, ProductsBlocState>(
          builder: (context, state) {
            return state.isLoading ?? true
                ? const Center(child: CircularProgressIndicator())
                : state.products?.isEmpty ?? true
                    ? Center(
                        child: Text("No Products",
                            style: textstyle_20.copyWith(color: Colors.black)),
                      )
                    : RefreshIndicator(
                        onRefresh: () async {
                          BlocProvider.of<ProductsBloc>(context)
                              .add(GetProductsEvent());
                        },
                        child: AnimationLimiter(
                          child: ListView.separated(
                            padding: const EdgeInsets.all(20),
                            itemCount: state.products!.length,
                            itemBuilder: (context, index) {
                              return AnimationConfiguration.staggeredList(
                                position: index,
                                delay: const Duration(milliseconds: 100),
                                child: SlideAnimation(
                                  duration: const Duration(milliseconds: 800),
                                  child: FadeInAnimation(
                                    duration: const Duration(seconds: 3),
                                    curve: Curves.fastLinearToSlowEaseIn,
                                    child: Bounce(
                                      duration:
                                          const Duration(milliseconds: 100),
                                      onPressed: () {
                                        context.push(
                                          context.namedLocation(
                                            'product_details',
                                            queryParameters: {
                                              'id': state.products![index].id
                                                  .toString()
                                            },
                                          ),
                                          extra: context.read<ProductsBloc>(),
                                        );
                                      },
                                      child: ProductCard(
                                        productEntity: state.products![index],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                            separatorBuilder: (context, index) => const Divider(
                              height: 40,
                              thickness: 1,
                            ),
                          ),
                        ),
                      );
          },
        ),
      ),
    );
  }
}
