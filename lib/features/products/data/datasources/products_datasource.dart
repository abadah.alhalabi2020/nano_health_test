import 'dart:developer';

import 'package:nano_health_test/core/constants/enums.dart';
import 'package:nano_health_test/features/products/data/models/product_model.dart';

import '../../../../core/apis/http_provider.dart';

abstract class ProductsDataResource {
  Future<List<ProductModel>?> getProducts();
  Future<ProductModel?> getProductDetails({required int id});
}

class ProductsDataResourceImpl implements ProductsDataResource {
  ProductsDataResourceImpl({required this.httpProvider});
  final HttpProvider httpProvider;

  @override
  Future<List<ProductModel>?> getProducts() async {
    try {
      var response = await httpProvider.goApi(
        url: 'products',
        method: ApiMethod.get,
      );
      inspect(response);
      if (response != null) {
        List<ProductModel> products = [];
        for (var item in response as List) {
          products.add(ProductModel.fromJson(item as Map<String, dynamic>));
        }
        return products;
      }
    } catch (e) {
      throw Exception(e);
    }
    return null;
  }

  @override
  Future<ProductModel?> getProductDetails({required int id}) async {
    try {
      var response = await httpProvider.goApi(
        url: 'products/$id',
        method: ApiMethod.get,
      );
      if (response != null) {
        return ProductModel.fromJson(response as Map<String, dynamic>);
      }
    } catch (e) {
      throw Exception(e);
    }
    return null;
  }
}
