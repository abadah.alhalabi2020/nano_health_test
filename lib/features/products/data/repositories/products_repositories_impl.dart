import 'package:nano_health_test/features/products/data/models/product_model.dart';

import '../../domain/repositories/Products_repositories.dart';
import '../datasources/Products_datasource.dart';

class ProductsRepositoriesImpl implements ProductsRepositories {
  ProductsDataResource productsDataResource;

  ProductsRepositoriesImpl({required this.productsDataResource});

  @override
  Future<List<ProductModel>?> getProducts() async {
    final response = await productsDataResource.getProducts();
    return response;
  }

  @override
  Future<ProductModel?> getProductDetails({required int id}) async {
    final response = await productsDataResource.getProductDetails(id: id);
    return response;
  }
}
