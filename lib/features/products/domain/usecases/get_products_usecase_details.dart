
import 'package:nano_health_test/features/products/domain/entities/product_entity.dart';
import 'package:nano_health_test/features/products/domain/repositories/Products_repositories.dart';


class GetProductsDetailsUsecase {
  final ProductsRepositories authRepositories;
  GetProductsDetailsUsecase(
    this.authRepositories,
  );
  Future<ProductEntity?> call({required int id}) async {
    try {
      return await authRepositories.getProductDetails(id: id);
    } catch (e) {
      throw Exception(e);
    }
  }
}