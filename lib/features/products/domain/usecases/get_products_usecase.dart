
import 'package:nano_health_test/features/products/domain/entities/product_entity.dart';
import 'package:nano_health_test/features/products/domain/repositories/Products_repositories.dart';


class GetProductsUsecase {
  final ProductsRepositories authRepositories;
  GetProductsUsecase(
    this.authRepositories,
  );
  Future<List<ProductEntity>?> call() async {
    try {
       return await authRepositories.getProducts();
    } catch (e) {
      throw Exception(e);
    }
  }
}