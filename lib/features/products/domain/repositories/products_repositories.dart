
import 'package:nano_health_test/features/products/data/models/product_model.dart';
import 'package:nano_health_test/features/products/domain/entities/product_entity.dart';

abstract class ProductsRepositories {
  Future<List<ProductEntity>?> getProducts();
  Future<ProductEntity?> getProductDetails({required int id});
}
